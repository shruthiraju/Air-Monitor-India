Android app that displays real time pollution data fetched from *Central Pollution Control Board India (data.gov.in)*.
**1,000+** downloads on the Google Play Store.

**View screenshots here:**
https://drive.google.com/file/d/173mcTuatAD0DsmnHO9aIA2HEStwOLSzp/view?usp=sharing
https://drive.google.com/file/d/1oKG_9KsSI0jyt_IdxIm_7aRT58tsJr2R/view?usp=sharing
https://drive.google.com/file/d/1NuRLlkngruULjnQDgp7v8ckH33pDe-Z4/view?usp=sharing